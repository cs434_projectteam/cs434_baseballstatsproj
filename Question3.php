<?php
require_once 'Setup.php';

try {
    $pdo = new PDO(PDO_CONNECT_STRING, DB_USER, DB_PWD);
    
    $getCountryCount = "SELECT Country, COUNT(Country) 
						FROM Player WHERE Country <> ''
						GROUP BY Country 
						ORDER BY COUNT(Country) DESC, State";
//echo "here";
    $countryCount = $pdo->query($getCountryCount); 
    while($result = $countryCount->fetch(PDO::FETCH_ASSOC)){
		$rows 	.= 	'<tr>
    					<td>'.$result['Country'].'</td>
    					<td>'.$result['COUNT(Country)'].'</td>
					</tr>';
    }

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $pdo = null;
    
    echo $rows;
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}


?>