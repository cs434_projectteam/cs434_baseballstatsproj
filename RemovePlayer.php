<?php

require_once 'Setup.php';

try {
	$pdo = new PDO(PDO_CONNECT_STRING, DB_USER, DB_PWD);

	$playerId = $_POST['playerId'];
	
	$removePlayer = "DELETE FROM Player WHERE PlayerID="."'".$playerId."'";

    $pdo->query($removePlayer);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $pdo = null;

    echo "Player Successfully Removed!";
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
?>