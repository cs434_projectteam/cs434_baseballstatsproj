<?php

require_once 'Setup.php';

try {
	$pdo = new PDO(PDO_CONNECT_STRING, DB_USER, DB_PWD);

	$playerId = generateRandomString();
	$playerName = $_POST['playerName'];
	$playerCountry = $_POST['playerCountry'];
	$playerState = $_POST['playerState'];
	$birthYear = $_POST['birthYear'];
	$deathYear = $_POST['deathYear'];
	$playerCollege = $_POST['playerCollege'];
	
	$insertPlayer = "INSERT INTO Player VALUES ("."'". $playerId ."'".", "."'". $playerName ."'".", "."'". $playerCountry ."'".", "."'". $playerState ."'".", "."'". $birthYear ."'".", "."'". $deathYear ."'".", "."'". $playerCollege ."'".")";

    $pdo->query($insertPlayer);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $pdo = null;

    echo "Player Successfully Inserted!";
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}

function generateRandomString() {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 7; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

?>