<?php
require_once 'Setup.php';

try {
	$pdo = new PDO(PDO_CONNECT_STRING, DB_USER, DB_PWD);

	$playerId = $_POST['playerId'];
	$playerName = $_POST['playerName'];
	$playerCountry = $_POST['playerCountry'];
	$playerState = $_POST['playerState'];
	$birthYear = $_POST['birthYear'];
	$deathYear = $_POST['deathYear'];
	$playerCollege = $_POST['playerCollege'];
	
	$updatePlayer = "UPDATE Player SET PlayerID="."'".$playerId."'".", Name="."'".$playerName."'".", Country="."'".$playerCountry."'".", State="."'".$playerState."'".", BirthYear="."'".$birthYear."'".", DeathYear="."'".$deathYear."'".", College="."'".$playerCollege."'"." WHERE PlayerID="."'".$playerId."'";

    $pdo->query($updatePlayer);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $pdo = null;

    echo "Player Successfully Updated!";
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
?>