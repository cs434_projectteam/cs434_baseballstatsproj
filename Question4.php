<?php
require_once 'Setup.php';

try {
    $pdo = new PDO(PDO_CONNECT_STRING, DB_USER, DB_PWD);
    
    $getTopTenColleges = "SELECT College, count(College)
    						FROM Player 
    						WHERE College <> 'None' AND College <> ''
    						GROUP BY College
    						ORDER BY count(College) DESC LIMIT 10";

    $topTenColleges = $pdo->query($getTopTenColleges); 
    while($result = $topTenColleges->fetch(PDO::FETCH_ASSOC)){
		$rows 	.= 	'<tr>
    					<td>'.$result['College'].'</td>
    					<td>'.$result['count(College)'].'</td>
					</tr>';
    }

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $pdo = null;
    
    echo $rows;
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}

?>