var resultArr = [];

// This is the show event for the Bootstrap Modal for
// either updating or removing a Player from the DB
$('#playerModal').on('show.bs.modal', function (event){
  var link = $(event.relatedTarget);
  var receiver = link.data('playerid');
  var modal = $(this);
  modal.find('#hiddenPlayerId').val(receiver);
});

// This is the SUBMIT event for the modal form;
// this will lead to the UpdatePlayer.php file
// where the information from the modal will 
// update the Player in the DB
$('#updateButton').on('click', function(event){
  var r = $('#hiddenPlayerId').val();
  var result = ValidateInsertInfo($('#updatePlayerTextbox').val());
  if(result){
    $.ajax({
      type: 'POST',
      url:'UpdatePlayer.php',
      data: 
      {
        playerId : $('#hiddenPlayerId').val(),
        playerName : resultArr[0],
        playerCountry : resultArr[1],
        playerState : resultArr[2],
        birthYear : parseInt(resultArr[3]),
        deathYear : parseInt(resultArr[4]),
        playerCollege : resultArr[5]
      },
      success: function (data) {
        // here the response can return either 'updated' or 'deleted'
        $('#playerModal').modal('hide');
        alert(data);
        $('#updatePlayerTextbox').val('');
      },
      error: function () {
          alert("There was an issue processing your request.  Try again.");
      }
    });
  }else{
    event.preventDefault();
    event.stopPropagation();
  }
});

/*********************DONE*********************/
// This is the SUBMIT event for the modal form;
// this will lead to the UpdatePlayer.php file
// where the information from the modal will 
// update the Player in the DB
$('#removeButton').on('click', function(event){
  $.ajax({
    type: 'POST',
    url:'RemovePlayer.php',
    data: 
    {
      playerId : $('#hiddenPlayerId').val()
    },
    complete: function (response) {
      // here the response can return either 'updated' or 'deleted'
      $('#playerModal').modal('hide');
      alert('Player successfully removed!');
      $('#updatePlayerTextbox').val('');
    },
    error: function () {
        alert("There was an issue processing your request.  Try again.");
    }
  });
});

// These are the functions for the Interesting Questions;
// the response from the AJAX request will append to the 
// 'answerLabel' DOM element
function q(i){
	   $.ajax({
      type: 'GET',
      url:'Question'+i+'.php',
      success: function (data) {

          // this is the dom element to add the php resonse to
          $('#answerLabel').html(data);
      },
      error: function () {
          $('div').html('Bummer: there was an error!');
      }
  });
}

/********************DONE*******************************/
// This function is used to Insert a player and calls the 
// InsertPlayer.php file
function InsertPlayer(){
  var result = ValidateInsertInfo($('#insertPlayerTextbox').val());
  if(result){
    $.ajax({
      type: 'POST',
      url:'PlayerInsert.php',
      data:
      {
        playerName : resultArr[0],
        playerCountry : resultArr[1],
        playerState : resultArr[2],
        birthYear : parseInt(resultArr[3]),
        deathYear : parseInt(resultArr[4]),
        playerCollege : resultArr[5]
      },
      success: function (data) {
          alert(data);
          $('#insertPlayerTextbox').val('');
      },
      error: function () {
          alert("There was an issue processing your request.  Try again.");
      }
    });
  }  
}

/********************DONE*******************************/
// This function is used to search for Players based
// on the name that is entered in the 'searchPlayerTextbox';
// this AJAX request uses the PlayerSearch.php file to return
// a set of rows containing the Players that were found
function SearchPlayer(){
  if(ValidateSearchInfo()){
    $.ajax({
      type: 'GET',
      url:'PlayerSearch.php',
      data: {
    	  name : $('#searchPlayerTextbox').val()
      },
      success: function (data) {
          // remove all existing rows from 'tableResults' table
          $('#resultsTable tbody tr').remove();

          // add new rows to 'tableResults' table
          $('#resultsTable tbody').append(data);
      },
      error: function () {
          alert("There was an issue processing your request.  Try again.");
      }
    });
  }  
}

// This function validates the insert information that is
// used to insert a Player in the DB
function ValidateInsertInfo(input){
  var result = true;
  resultArr = input.split(",");
  if(resultArr.length != 6 
    || resultArr[0] == '' 
    || resultArr[1] == '' 
    || resultArr[3] == '' 
    || !VerifyDates()){
    alert("Please enter the correctly formatted information.");
    result = false;
  }
  return result;
}

// This function verifies the dates are formatted correctly 
// based on the DB's constraints on the dates; **dates can
// only range from 1871 - 2012 and a deathYear cannot be less
// than a birthYear**
function VerifyDates(){
  var result = true;
  var birthYear = parseInt(resultArr[3])
  if(resultArr[4] != ' '){
    var deathYear = parseInt(resultArr[4]);
    if(!/^(187[0-9]|18[6-9]\d|19\d\d|200\d|201[0-2])$/g.test(birthYear)
      || !/^(187[0-9]|18[6-9]\d|19\d\d|200\d|201[0-2])$/g.test(deathYear) 
        || deathYear < birthYear){
      result = false;
    }
  }else{
    if(!/^(187[0-9]|18[6-9]\d|19\d\d|200\d|201[0-2])$/g.test(birthYear)){
      result = false;
    }
  }
  return result;
}

// This function validates the search information (Player Name)
// that is entered; **only alpha and space characters**
function ValidateSearchInfo(){
  var result = true;
  var input = $('#searchPlayerTextbox').val();
    if (!/^[a-zA-Z\s]*$/g.test(input) ||  input == '') {
        alert("Invalid Entry.  Please Enter a Player's Name.");
        result = false;
    }
    return result;
}