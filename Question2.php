<?php

require_once 'Setup.php';

try {
    $pdo = new PDO(PDO_CONNECT_STRING, DB_USER, DB_PWD);
    
    $getAvgSalaryWithCollege = "SELECT avg(s.DollarAmount)
						FROM Player p, Salary s 
						WHERE p.PlayerID = s.PlayerID AND s.DollarAmount > 0 AND p.College <> ''";

    $salaryWithCollege = $pdo->query($getAvgSalaryWithCollege); 
    $resultOne = $salaryWithCollege->fetch(PDO::FETCH_ASSOC);


    $getAvgSalaryWithoutCollege = "SELECT avg(s1.DollarAmount)
						FROM Player p1, Salary s1 
						WHERE p1.PlayerID = s1.PlayerID AND s1.DollarAmount > 0 AND p1.College= ''";

    $salaryWithoutCollege = $pdo->query($getAvgSalaryWithoutCollege); 
    $resultTwo = $salaryWithoutCollege->fetch(PDO::FETCH_ASSOC);


    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $pdo = null;

    echo nl2br("Average Salary w/ College: $" .round($resultOne['avg(s.DollarAmount)'], 2)."\r\n 
    		Average Salary w/out College: $" .round($resultTwo['avg(s1.DollarAmount)'], 2)."");
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}

?>