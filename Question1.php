<?php
require_once 'Setup.php';

try {
    $pdo = new PDO(PDO_CONNECT_STRING, DB_USER, DB_PWD);
    
    $getAvgLifespan = "SELECT avg(DeathYear - BirthYear) 
						FROM Player 
						WHERE DeathYear > 0 AND BirthYear > 0";

    $lifespan = $pdo->query($getAvgLifespan); 
    $result = $lifespan->fetch(PDO::FETCH_ASSOC);
    // ['PlayerID'], ['Name'], ['Country'], ['State'], ['BirthYear'], ['DeathYear'], ['College']

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $pdo = null;
    //die($rows);
    echo round($result['avg(DeathYear - BirthYear)'], 1)." years old";
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
?>